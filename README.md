# Lab 1 Vynohradova
This is the _Java Enterprise_ Project. There are different examples of using
the servlets and reading the data from the form inputs.

##Requirements
* Targeted java version is _1.8.0_301_.
* Download apache-tomcat (preferably version 8 or 9)

## Running the Project
Make sure to point your IDE of choice to its location when you're setting
run configuration (as an Application Server). Once it's done create two artifacts
under the deployment section.

##Licence
This project is under the MIT Licence. Anyone can feel free to use it. 
I chose it based on [this resource](https://gist.github.com/nicolasdao/a7adda51f2f185e8d2700e1573d8a633).