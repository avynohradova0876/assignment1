<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
  <title>JSP - Hello World</title>
</head>
<body>
<h1>Example 1</h1>
<h2><%= "Hello World!" %></h2>
<br/>
<a href="hello-servlet">Hello Servlet</a>
<br/>
<br/>
<h1>Example 2</h1>
<form method="post" action="form-handler-servlet">
    <table>
        <tr>
            <td>Please enter some text:</td>
            <td>
                <input type="text" name="enteredValue"/>
            </td>
        </tr>
        <tr>
            <td></td>
            <td><input type="submit" value="Submit"></td>
        </tr>
    </table>
</form>
<br/>
<br/>
<h1>Example 3</h1>
<form method="post" action="multiple-value-field-handler-servlet">
    <p>Please enter one or more options.</p>
    <table>
        <tr>
            <td><input name="options" type="checkbox" value="option1"/>
                Option 1
            </td>
        </tr>
        <tr>
            <td><input name="options" type="checkbox" value="option2"/>
                Option 2
            </td>
        </tr>
        <tr>
            <td><input name="options" type="checkbox" value="option3"/>
                Option 3
            </td>
        </tr>
        <tr>
            <td><input type="submit" value="Submit"/></td>
        </tr>
    </table>
</form>
<br/>
<br/>
<h1>Example 4</h1>
<form method="post" action="response-redirection-servlet">
    <p>Please indicate your favorite search engine.</p>
    <table>
        <tr>
            <td><input type="radio" name="searchEngine"
                       value="http://www.google.com">Google
            </td>
        </tr>
        <tr>
            <td><input type="radio" name="searchEngine"
                       value="http://www.bing.com">Bing
            </td>
        </tr>
        <tr>
            <td><input type="radio" name="searchEngine"
                       value="http://www.yahoo.com">Yahoo!
            </td>
        </tr>
        <tr>
            <td><input type="submit" value="Submit"/></td>
        </tr>
    </table>
</form>
</body>
</html>